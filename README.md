
e3-ts2evm - Test Stand 2 Event Master (EVM) IOC 
======
## Description
ts2evm is an E3 module dedicated to Test Stand 2 Timing System. It runs on top of the **mrfioc2** support for the MRF MTCA Event Master (mTCA-EVM-300). This module is intended to be used by the IOC that controls TS2 EVM.
## Operation

ts2evm will configure the EVM sequencer tables in order to periodically generate the main timing events for the TS2 operation:
- Modulator Start (Event 20)
- RF Start (Event 15)

The generation of these events in time domain are defined by high level parameters:
- Cycle Frequency
- RF start delay from modulator start
- RF pulse width

The module also allows two modes of operation for the EVM events:
- Free Running (with periods defined the cycle frequency - max. 14 Hz)
- On Demand (Discrete number of cycles)

## Implementation

The business logic of this module is implemented using EPICS database logic and aSub records for the manipulation of the EVM sequencer arrays.

The main concept of this module is bases on the [ESS SuperCycle](https://github.com/icshwi/supercycleEngine.git) engine:

## Dependencies
- [mrfioc2](https://gitlab.esss.lu.se/e3/wrappers/ts/e3-mrfioc2.git)
- [busy](https://gitlab.esss.lu.se/e3/wrappers/core/e3-busy.git)
## OPI Demonstration
![alt text](opi/evm-opi.gif "TS2 EVM Demo")
