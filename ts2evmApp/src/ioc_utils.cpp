/**
 * @file ioc_utils.cpp
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @brief 
 * @version 0.1
 * @date 2022-08-22
 * 
 * @copyright Copyright (c) 2022 European Spallation Source ERIC
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <iostream>
#include <string>
#include <vector>
#include <utility>

#include <pv/pvDatabase.h>
#include <pv/channelProviderLocal.h>

#include	<dbStaticLib.h>
#include	<dbAccess.h>	    /* includes dbDefs.h, dbBase.h, dbAddr.h, dbFldTypes.h */
#include	<recSup.h>		    /* rset */
#include	<dbConvert.h> 	    /* dbPutConvertRoutine */
#include	<dbConvertFast.h>	/* dbFastPutConvertRoutine */
#include	<initHooks.h>
#include	<epicsThread.h>
#include	<errlog.h>
#include	<iocsh.h>
#include	<special.h>
#include	<macLib.h>
#include	<epicsString.h>
#include	<dbAccessDefs.h>
#include	<epicsStdio.h>

#include <iocsh.h>
#include <initHooks.h>

// The following must be the last include for code database uses
#include <epicsExport.h>

#define epicsExportSharedSymbols
#include "ntTable.h"

using namespace std;
using namespace epics::pvData;
using namespace epics::pvDatabase;

bool configureFunctionCalled = false;

void evmTableHooks(initHookState state)
{ 
    if(state==initHookAfterIocRunning) {
        configureFunctionCalled = true; 
    }
}

/***********************************************************************/

static const iocshArg funcArg0 = { "NTTable PV Name", iocshArgString };
static const iocshArg *funcArgs[] = {&funcArg0};
static const iocshFuncDef evmTableFuncDef = {"evmConfigNTTable", 1, funcArgs};

static void evmTableCallFunc(const iocshArgBuf *args)
{
    if (!configureFunctionCalled) {
        if(!args[0].sval) {
            throw std::runtime_error("evmConfigNTTable: missing argument. Usage: evmConfigNTTable [PREFIX]");
        }

        // Get main prefix of the IOC records and adds the standard suffix "EvtTable"
        string ntTableRecordName(args[0].sval);
        ntTableRecordName += "EvtTable";
        
        // Create record with custom PVRecord class and add it to IOC
        ntTableEvmRecordPtr record = ntTableEvmRecord::create(ntTableRecordName);
        bool result = PVDatabase::getMaster()->addRecord(record);
        if(!result) 
            errlogPrintf("evmConfigNTTable: NTTable record %s was not added to the IOC\n", ntTableRecordName.c_str());

        configureFunctionCalled = true; 


    } else {
        errlogPrintf("evmConfigNTTable: IOC already running, this function call causes no effect\n");
    }
    
}

static void evmTableRegister(void)
{
    static int firstTime = 1;
    if (firstTime) {
        configureFunctionCalled = false;
        iocshRegister(&evmTableFuncDef, evmTableCallFunc);
        initHookRegister(&evmTableHooks);
    }
}

extern "C" {
    epicsExportRegistrar(evmTableRegister);
}
