/**
 * @file evm_subroutines.c
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @author Gabriel Fedel (gabriel.fedel@ess.eu)
 * @brief 
 * @version 0.3
 * @date 2022-08-18
 * 
 * @copyright Copyright (c) ESS 2020
 * 
 * The code below is based on supercycleEngine project:
 * https://github.com/icshwi/supercycleengine
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <epicsTypes.h>

#include <string.h>

//------------------------------------------------------------------------
//  Event Codes
//------------------------------------------------------------------------
#define EVT14HZ     14
#define PIEZZO_ST   22
#define LLRF_ST     21
#define RF_ST       15
#define RF_END      16
#define BPULSE_ST   12
#define BPULSE_END  13
#define MOD_TRIG    20
#define SEQ_END     127

#define PRETRIGGER_DELAY 10000.0
#define MAX_RF_DELAY    (30000.0 - PRETRIGGER_DELAY - 10.0)
#define MAX_RF_WIDTH    3500.0

#define MAX_SEQ_EVENTS  8

//------------------------------------------------------------------------
//  Routine that writes the EVM sequencer events and timestamps arrays 
//------------------------------------------------------------------------
static long seqConfigure(aSubRecord *prec)
{
    int   *event_codes;
    double *timestamps;

    // Get RF pulse width from INPA
    float rf_width = *(float *)prec->a;
    
    // Get Proton Beam pulse width from INPB
    float pb_width = *(float *)prec->b;

    // Get delays
    float rf_delay = *(float *)prec->c; 
    float pb_delay = *(float *)prec->d;
    float llrf_delay = *(float *)prec->g;
    float piezzo_delay = *(float *)prec->h;

    // Get cycle period and tick value
    const float cycle_period = (1.0 / *(float *)prec->e) * 1000000.0;   // period in microseconds
    const float tick_value   = (1.0 / *(float *)prec->i);               // tick value in microseconds

    // Validate RF width 
    rf_width = (rf_width < 4.0) ? 4.0 : rf_width;
    rf_width = (rf_width >= MAX_RF_WIDTH ? MAX_RF_WIDTH : rf_width);   

    // Validate RF delay
    rf_delay = (rf_delay < (2*tick_value)) ? (2*tick_value) : rf_delay;
    if ((PRETRIGGER_DELAY + rf_delay + rf_width) >= (cycle_period)) {
        rf_delay = cycle_period - PRETRIGGER_DELAY - rf_width - (2*tick_value);
    }

    // Validate LLRF delay
    llrf_delay = ((rf_delay - llrf_delay) < tick_value) ? tick_value : llrf_delay;

    // Validate piezzo trigger
    piezzo_delay = (piezzo_delay >= PRETRIGGER_DELAY) ? (PRETRIGGER_DELAY - tick_value) : piezzo_delay;

    // Validate PB Pulse
    pb_width = (pb_width < tick_value) ? tick_value : pb_width;
    pb_width = (pb_width >= rf_width) ? (rf_width - (2*tick_value)) : pb_width;
    pb_delay = (pb_delay < tick_value) ? tick_value : pb_delay;
    pb_delay = ((pb_delay+pb_width) >= rf_width) ? (rf_width - pb_width - tick_value) : pb_delay;

    event_codes = (int*) malloc(MAX_SEQ_EVENTS * sizeof(int));
    timestamps = (double*) malloc(MAX_SEQ_EVENTS * sizeof(double));
    
    // Create the array of events
    event_codes[0] = PIEZZO_ST;
    event_codes[1] = MOD_TRIG;
    event_codes[2] = LLRF_ST;
    event_codes[3] = RF_ST;
    event_codes[4] = BPULSE_ST;
    event_codes[5] = BPULSE_END;
    event_codes[6] = RF_END;
    event_codes[7] = SEQ_END;
    
    // Create the array of time stamps
    timestamps[0] = piezzo_delay;               // PIEZZO_ST 
    timestamps[1] = PRETRIGGER_DELAY;           // MOD_TRIG
    timestamps[2] = timestamps[1] + rf_delay - llrf_delay;   // LLRF_ST
    timestamps[3] = timestamps[1] + rf_delay;   // RF_ST
    timestamps[4] = timestamps[3] + pb_delay;   // BPULSE_ST
    timestamps[5] = timestamps[4] + pb_width;   // BPULSE_END
    timestamps[6] = timestamps[3] + rf_width;   // RF_END
    timestamps[7] = timestamps[6] + tick_value; // sequencer re-arm

    //printf("(waveform write start)--------------------------\n");
    //printf("RF delay = %.6f\n", rf_delay);
    //printf("RF width = %.6f\n", rf_width);
    //printf("(waveform write end)--------------------------\n");

    // Write the output arrays
    memcpy(prec->vala, event_codes, MAX_SEQ_EVENTS * sizeof(int));
    prec->neva = MAX_SEQ_EVENTS;
    memcpy(prec->valb, timestamps, MAX_SEQ_EVENTS * sizeof(double));
    prec->nevb = MAX_SEQ_EVENTS;

    free(event_codes);
    free(timestamps);

    return 0;
}
epicsRegisterFunction(seqConfigure);


//------------------------------------------------------------------------
//  Routine that verifies the EVM sequencer events and timestamps arrays 
//    timestamps[0] = PIEZZO_ST
//    timestamps[1] = MOD_TRIG
//    timestamps[2] = LLRF_ST
//    timestamps[3] = RF_ST
//    timestamps[4] = BPULSE_ST
//    timestamps[5] = BPULSE_END
//    timestamps[6] = RF_END
//    timestamps[7] = sequencer re-arm
//------------------------------------------------------------------------
static long seqVerify(aSubRecord *prec)
{
    // auxiliary var
    float timestamps[MAX_SEQ_EVENTS];
    
    // Get timestamps array from TS2 "shadow" waveform
    //float *timestamps_rb = (float *)prec->a;

    // Get timestamps array from SoftSeq0 readback waveform
    float *timestamps_seq0_rb = (float *)prec->b;

    // Get timestamps array from SoftSeq1 readback waveform
    float *timestamps_seq1_rb = (float *)prec->c;

    // tick value in microseconds
    //const float tick_value   = (1.0 / *(float *)prec->d);

    // sequencer selector
    int const seq_sel = *((int*)prec->e);

    // Copy desidred values to auxiliary array
    if (seq_sel == 0) {
        memcpy(timestamps, timestamps_seq0_rb, MAX_SEQ_EVENTS*sizeof(float));
    } else {
        memcpy(timestamps, timestamps_seq1_rb, MAX_SEQ_EVENTS*sizeof(float));
    }

    // Calculate RF delay (RF_ST - MOD_TRIG)
    const float rf_delay = timestamps[3] - timestamps[1];

    // Calculate RF width (RF_END - RF_ST)
    const float rf_width = timestamps[6] - timestamps[3];

    // Calculate PB delay (BPULSE_ST - RF_ST)
    const float pb_delay = timestamps[4] - timestamps[3];

    // Calculate PB width (BPULSE_END - BPPULSE_ST)
    const float pb_width = timestamps[5] - timestamps[4];

    //printf("(readback start)--------------------------\n");
    //printf("RF delay = %.6f\n", rf_delay);
    //printf("RF width = %.6f\n", rf_width);
    //printf("(readback end)--------------------------\n");

    // Write the readback legth PVs
    memcpy(prec->vala, &rf_width, sizeof(float));
    prec->neva = 1;
    memcpy(prec->valb, &pb_width, sizeof(float));
    prec->nevb = 1;
    memcpy(prec->valc, &rf_delay, sizeof(float));
    prec->nevc = 1;
    memcpy(prec->vald, &pb_delay, sizeof(float));
    prec->nevd = 1;
    
    return 0;
}
epicsRegisterFunction(seqVerify);


//---------------------------------------------------------------
// Waveform copies
//---------------------------------------------------------------

static long seq_table_copy(aSubRecord *prec) {
    //Load input waveform and sequencer selection
    double *timestampin = (double*)prec->a;
    unsigned long *evt_in = (unsigned long*)prec->b;
    //int const seq_sel = *((int*)prec->c);

    //printf("[DEBUG] writing SoftSeq-[0,1]-Timestamp and EvtCode, with selection = %d\n", seq_sel);
    memcpy((double *) prec->vala, timestampin, 8 * sizeof(double));
    memcpy((unsigned long *) prec->valb, evt_in, 8 * sizeof(unsigned long));
    //set size of the output
    prec->neva = 8;
    prec->nevb = 8;
    
    return 0;
}

static long seq_table_get(aSubRecord *prec) {

    //Load input waveform and sequencer selection
    double *timestampin_a = (double*)prec->a;
    double *timestampin_b = (double*)prec->b;
    unsigned long *evtin_a = (unsigned long*)prec->c;
    unsigned long *evtin_b = (unsigned long*)prec->d;
    long const seq_sel = *((long*)prec->e);

    //printf("[DEBUG] Reading SoftSeq-[0,1]-Timestamp and EvtCode, with selection = %d\n", seq_sel);
    if (seq_sel == 0) { 
        //copy waveform from sequencer 0
        memcpy((double *) prec->vala, timestampin_a, 8 * sizeof(double));
        memcpy((unsigned long *) prec->valb, evtin_a, 8 * sizeof(unsigned long));
        //set size of the output
        prec->neva = 8;
    } else {
        //copy waveform from sequencer 1
        memcpy((double *) prec->vala, timestampin_b, 8 * sizeof(double));
        memcpy((unsigned long *) prec->valb, evtin_b, 8 * sizeof(unsigned long));
        //set size of the output
        prec->nevb = 8;
    }
    return 0;
}

epicsRegisterFunction(seq_table_copy);
epicsRegisterFunction(seq_table_get);
