/**
 * @file ntTable.cpp
 * @author Joao Paulo Martins (joaopaulo.martins@ess.eu)
 * @brief 
 * @version 0.1
 * @date 2022-08-22
 * 
 * @copyright Copyright (c) 2022 European Spallation Source ERIC
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <map>

#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <epicsTypes.h>
#include <errlog.h>

#include <cmath>
#include <pv/pvDatabase.h>
#include <pv/standardField.h>

#define epicsExportSharedSymbols
#include "ntTable.h"

using namespace epics::pvData;
using namespace epics::pvDatabase;
namespace pvd = epics::pvData;
using std::tr1::static_pointer_cast;
using std::string;
using std::vector;
using std::cout;
using std::map;

static map<int, string> ts2Events = {   
    { 14,  "14Hz Fiducial"},
    { 22,  "Piezzo Start"},
    { 20,  "Modulator Start"},
    { 21,  "LLRF Start"},
    { 15,  "RF Start"},
    { 16,  "RF End"},
    { 12,  "Proton Beam Start"},
    { 13,  "Proton Beam End"},
    { 127, "EVM Sequencer Reset"},
};

ntTableEvmRecordPtr ntTableEvmRecord::create(string const & recordName)
{
    StandardFieldPtr standardField = getStandardField();
    FieldCreatePtr fieldCreate = getFieldCreate();
    PVDataCreatePtr pvDataCreate = getPVDataCreate();

    // Standard NTTable record with 3 columns
    StructureConstPtr topStructure(getFieldCreate()->createFieldBuilder()
                                ->setId("epics:nt/NTTable:1.0")
                                ->addNestedStructure("evm_sequencer")
                                    ->addArray("EventName", pvd::pvString)
                                    ->addArray("EventCode", pvd::pvInt)
                                    ->addArray("Timestamp", pvd::pvFloat)
                                ->endNested()
                                ->createStructure());

    PVStructurePtr pvStructure = pvDataCreate->createPVStructure(topStructure);
    ntTableEvmRecordPtr pvRecord(new ntTableEvmRecord(recordName,pvStructure));
    
    pvRecord->initPVRecord();
    return pvRecord;
}

ntTableEvmRecord::ntTableEvmRecord(
    string const & recordName,
    PVStructurePtr const & pvStructure)
: PVRecord(recordName,pvStructure)
{}

void ntTableEvmRecord::updateTable() {
    errlogPrintf("[DEBUG] process ntTable - %s\n", getRecordName().c_str());
}

void ntTableEvmRecord::process()
{
    // prevent any external "put" operation to change the value of the record
    updateTable();
}


//------------------------------------------------------------------------
//  aSub process to write data buffer
//------------------------------------------------------------------------
static long processNtTable(aSubRecord *prec)
{
    const char* ntTablePvPrefix = (const char*)(prec->a);
    std::string ttstr(ntTablePvPrefix);
    ttstr += "EvtTable";
   
    // Get timestamps array from TS2 readback waveform
    epicsFloat32 *timestamps = (epicsFloat32 *)prec->b;
    size_t nelmts = (size_t)prec->neb;

    // Get event codes array from TS2 readback waveform
    epicsUInt32 *evtcode = (epicsUInt32 *)prec->c;
    size_t nelmevt = (size_t)prec->nec;

    //Checks if arrays have the same size
    if (nelmevt != nelmts) {
        return -1;
    }

    // tries to find NTTable record
    PVRecordPtr evmTable = PVDatabase::getMaster()->findRecord(ttstr);

    if (evmTable != nullptr) {
        size_t i;
        shared_vector<string> evtNamesArray;
        shared_vector<int> evtCodesArray;
        shared_vector<float> timeStampsArray;
        for (i = 0; i < nelmts; i++) {
            evtCodesArray.push_back((int)evtcode[i]);
            timeStampsArray.push_back((float)timestamps[i]);
            evtNamesArray.push_back(ts2Events[evtcode[i]]);
        }

        try {
            // Update the record
            PVStructurePtr pvStructure = evmTable->getPVStructure();
        
            // Copy event names array
            PVStringArrayPtr evtNamesArray_ = pvStructure->getSubField<PVStringArray>("evm_sequencer.EventName");
            evtNamesArray_->putFrom(freeze(evtNamesArray));

            // Copy event codes array
            PVIntArrayPtr evtCodesArray_ = pvStructure->getSubField<PVIntArray>("evm_sequencer.EventCode");
            evtCodesArray_->putFrom(freeze(evtCodesArray));

            // Copy timestamps array
            PVFloatArrayPtr timeStampsArray_ = pvStructure->getSubField<PVFloatArray>("evm_sequencer.Timestamp");
            timeStampsArray_->putFrom(freeze(timeStampsArray));
        
        } catch(std::exception& e){
            errlogPrintf("[ERROR] %s: %s\n", evmTable->getRecordName().c_str(), e.what());
            return -1;
        }
        return 0;

    } else {
        errlogPrintf("[ERROR] %s: Couldn't find NTTable PV %s\n", prec->name, ttstr.c_str());
        return -1;
    }
}

extern "C"
{
    epicsRegisterFunction(processNtTable);
}
