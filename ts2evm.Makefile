#
# Copyright (c) 2019    European Spallation Source ERIC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# 
# Author  : JoaoPaulo Martins
# email   : joaopaulo.martins@esss.se
# Date    : 2022Aug17-1448-00CEST
# version : 1.3.0 
#

# The following lines are required
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


# Not needed for PPC
EXCLUDE_ARCHS += linux-ppc64e6500

REQUIRED += busy
ifneq ($(strip $(BUSY_DEP_VERSION)),)
  busy_VERSION=$(BUSY_DEP_VERSION)
endif

REQUIRED += mrfioc2
ifneq ($(strip $(MRFIOC2_DEP_VERSION)),)
  mrfioc2_VERSION=$(MRFIOC2_DEP_VERSION)
endif


# Since this file (ts2evm.Makefile) is copied into
# the module directory at build-time, these paths have to be relative
# to that path
APP := .
APPDB := $(APP)/Db
APPSRC := $(APP)/src

SOURCES += $(APPSRC)/evm_subroutines.c
SOURCES += $(APPSRC)/databuffer_subroutines.cpp
SOURCES += $(APPSRC)/ntTable.cpp
SOURCES += $(APPSRC)/ioc_utils.cpp
TEMPLATES += $(wildcard $(APPDB)/*.template)
DBDS += $(APPSRC)/ts2evm_definitions.dbd

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

# Same as with any source or header files, you can also use $SUBS and $TMPS to define
# database files to be inflated (using MSI), e.g.
#
#     SUBS = $(wildcard $(APPDB)/*.substitutions)
#     TMPS = $(wildcard $(APPDB)/*.template)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)

.PHONY: vlibs
vlibs:

